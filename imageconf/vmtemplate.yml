---
name: whstack-percona-xtradb-cluster56-vm
provider:
  name: kvm
bootstrapper:
  workspace: /target/os-images
  mirror: https://debian-archive.packages.managed-infra.com/debian/
  include_packages:
   - initramfs-tools
   - linux-headers-amd64
   - whois
   - psmisc
   - apt-transport-https
   - ca-certificates
   - ssl-cert
system:
  release: stretch
  architecture: amd64
  hostname: whstack-percona-xtradb-cluster56-vm
  bootloader: grub
  charmap: UTF-8
  locale: en_US
  timezone: Europe/Berlin
volume:
  backing: qcow2
  partitions:
    type: gpt
    boot:
      filesystem: ext4
      size: 1GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    swap:
      size: 2GiB
      mountopts:
        - sw
        - discard
        - noatime
        - errors=remount-ro
    root:
      filesystem: ext4
      size: 9GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
    var/log:
      filesystem: ext4
      size: 2GiB
      mountopts:
        - defaults
        - discard
        - noatime
        - errors=remount-ro
packages:
  install:
    # custom standard packages
    - chrony
    - unp
    - locate
    - lsscsi
    - sg3-utils
    - unattended-upgrades
    - debsecan
    - vim
    - nano
    - vim-nox
    - unzip
    - rsync
    - davfs2
    - bzip2
    - zstd
    - binutils
    - sudo
    - anacron
    - incron
    - screen
    - tree
    - haveged
    - needrestart
    - rng-tools
    - resolvconf
    - wget
    - curl
    # network tools
    - ethtool
    - wpasupplicant
    - mstflint
    - iproute2
    - dante-client
    - proxychains
    - net-tools
    - mtr
    - traceroute
    - ebtables
    - tcpdump
    - snmpd
    - snmp
    - snmp-mibs-downloader
    - lldpd
    - ipxe
    - ipxe-qemu
    - memtest86+
    # backup
    - borgbackup
    - proxmox-backup-client
    - etckeeper
    # email smarthost
    #- dma [exim4-daemon-light : Conflicts: mail-transport-agent]
    - mailutils
    - mutt
    # cloud specific packages
    - cloud-init
    - salt-minion
    - puppet-agent
    - chef
    - qemu-guest-agent
    - open-vm-tools
    # user access management (directory) [not available for deb stretch]
    #- freeipa-client
    - sssd
    - sssd-dbus
    - libsss-sudo
    - libsss-idmap0
    - libsss-nss-idmap0
    - krb5-config
    - krb5-k5tls
    - krb5-user
    # - libpam-krb5
    - libpam-ccreds
    # monitoring
    - lm-sensors
    - nvme-cli
    - apcupsd
    - smartmontools
    - monit
    - icinga2
    - lynis
    - trivy
    - fio
    - zabbix-agent
    - omi
    - scx
    - watchdog
    - monitoring-plugins
    - monitoring-plugins-basic
    - monitoring-plugins-standard
    - monitoring-plugins-btrfs
    - nagios-plugins-contrib
    - nagios-snmp-plugins
    - nagios-plugins-rabbitmq
    - nagios-check-xmppng
    - libmonitoring-plugin-perl
    - libipc-run-perl
    - liblist-moreutils-perl
    - filebeat
    - metricbeat
    - auditbeat
    - heartbeat-elastic
    - packetbeat
    # security
    - cracklib-runtime
    - wazuh-agent
    - auditd
    - audispd-plugins
    - ipset
    - conntrack
    - shorewall
    - shorewall6
    - rkhunter
    - fail2ban
    - crowdsec
    - crowdsec-firewall-bouncer-iptables
    # system management
    - aptitude
    - rpm
    - apt-file
    - apt-show-versions
    - apt-listchanges
    - debsums
    - atop
    - iotop
    - nload
    # cluster tools
    - pacemaker
    - heartbeat
    - corosync
    - keepalived
    - exabgp
    - python3
    - python3-dev
    - python3-venv
    - python3-pip
    - python3-setuptools
    - python3-wheel
    - python
    - python-pip
    - python-setuptools
    # Percona packages
    - percona-xtradb-cluster-full-56
    - adminer
    - composer
    - yarn
    # administration
    #- webmin
  install_standard: false
  mirror: https://debian-archive.packages.managed-infra.com/debian/
  security: https://debian-archive.packages.managed-infra.com/debian-security/
  sources:
    debian-backports:
      - deb https://debian-archive.packages.managed-infra.com/debian stretch-backports main contrib non-free
    pbs-client:
      - deb https://proxmox.packages.managed-infra.com/debian/pbs-client stretch main
    icinga:
      - deb https://icinga.packages.managed-infra.com/debian icinga-stretch main
    zabbix:
      - deb https://zabbix.packages.managed-infra.com/zabbix/6.5/debian stretch main
      - deb-src https://zabbix.packages.managed-infra.com/zabbix/6.5/debian stretch main
    microsoft:
      - deb https://microsoft.packages.managed-infra.com/debian/9/prod stretch main
    aquasecurity:
      - deb https://aquasecurity.packages.managed-infra.com/trivy-repo/deb stretch main
    crowdsec:
      - deb https://packagecloud.packages.managed-infra.com/crowdsec/crowdsec/any/ any main
    newrelic-infra:
      - deb https://newrelic.packages.managed-infra.com/infrastructure_agent/linux/apt stretch main
    webmin:
      - deb https://webmin.packages.managed-infra.com/download/repository sarge contrib
    elasticsearch:
      - deb https://elastic.packages.managed-infra.com/packages/8.x/apt stable main
    puppet:
      - deb https://puppetlabs-apt.packages.managed-infra.com stretch puppet
    chef:
      - deb https://chef.packages.managed-infra.com/repos/apt/stable stretch main
    saltstack:
      - deb https://saltstack.packages.managed-infra.com/artifactory/saltproject-deb/ stable main
    percona:
      - deb https://percona.packages.managed-infra.com/percona/apt stretch main
      - deb-src https://percona.packages.managed-infra.com/percona/apt stretch main
      - deb https://percona.packages.managed-infra.com/tools/apt stretch main
      - deb-src https://percona.packages.managed-infra.com/tools/apt stretch main
    yarnpkg:
      - deb https://yarnpkg.packages.managed-infra.com/debian/ stable main
    wazuh:
      - deb https://wazuh.packages.managed-infra.com/4.x/apt/ stable main
    cisofy:
      - deb https://cisofy.packages.managed-infra.com/community/lynis/deb/ stable main
    falcosecurity:
      - deb https://falco.packages.managed-infra.com/packages/deb stable main
  components:
    - main
    - contrib
    - non-free
  trusted-keys:
    - trusted-repo-keys/trusted-repo-keys.gpg
  apt.conf.d:
    00InstallRecommends: >-
      APT::Install-Recommends "false";
      APT::Install-Suggests   "false";
    00RetryDownload: 'APT::Acquire::Retries "3";'
plugins:
  tmpfs_workspace: {}
  admin_user:
    username: admin
    password: ch4ng3m3
    pubkey: ../authorized_keys
  apt_proxy:
    address: 127.0.0.1
    port: 8000
  debconf: >-
    d-i pkgsel/install-language-support boolean false
    popularity-contest popularity-contest/participate boolean false
    resolvconf resolvconf/linkify-resolvconf boolean true
  file_copy:
    files:
      - { src: ../copy-to-filesystem.tar.gz,
          dst: /opt/copy-to-filesystem.tar.gz,
          permissions: -rwxrwxrwx,
          owner: root,
          group: root }
  commands:
    commands:
      - ['chroot {root} tar --strip-components 1 -xvzf /opt/copy-to-filesystem.tar.gz -C /']
      - ['chroot {root} chmod 770 /usr/sbin/cmdb-deploy']
      - ['chroot {root} chmod 770 /usr/bin/cleanup_image']
      - ['chroot {root} /usr/bin/cleanup_image']
  minimize_size:
    zerofree: true
    apt:
      autoclean: true
